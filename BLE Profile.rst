Mechanical
==========

Theoretical profile
-------------------

::

	Handle  Description
	 0000   Service: Generic Access (1800)
	 0001   Characteristic: Device Name (2A00, read)
	 0002   Characteristic Value (string)
	 0003   Characteristic: Appearance (2A01, read)
	 0004   Characteristic Value (16bit enum)
	 0005   Service: Generic Attribute (1801)
	 0006   Service: Prosthetic Flex Status Service (e35c8bac-a062-4e3f-856d-2cfa87f2f171)
	 0007   Characteristic: Prosthetic Flex Status Characteristic (e35c8910-a062-4e3f-856d-2cfa87f2f171, read, notify)
	 0008   Characteristic Value (hex, 20 bytes) # TODO: we don't actually need 20b here

Actual impl in telemetry.node-gatt-server
-----------------------------------------

.. code:: json

        {
          "type": "service",
          "uuid": "1800",
          "startHandle": 1,
          "endHandle": 5
        },
        {
          "type": "characteristic",
          "uuid": "2a00",
          "properties": 2,
          "secure": 0,
          "startHandle": 2,
          "valueHandle": 3
        },
        {
          "type": "characteristicValue",
          "handle": 3,
        },
        {
          "type": "characteristic",
          "uuid": "2a01",
          "properties": 2,
          "secure": 0,
          "startHandle": 4,
          "valueHandle": 5
        },
        {
          "type": "characteristicValue",
          "handle": 5,
        },
        {
          "type": "service",
          "uuid": "1801",
          "startHandle": 6,
          "endHandle": 9
        },
        {
          "type": "characteristic",
          "uuid": "2a05",
          "properties": 32,
          "secure": 0,
          "startHandle": 7,
          "valueHandle": 8
        },
        {
          "type": "characteristicValue",
          "handle": 8,
        },
        {
          "type": "descriptor",
          "handle": 9,
          "uuid": "2902",
          "properties": 14,
          "secure": 0,
        },
        {
          "type": "service",
          "uuid": "e35c8baca0624e3f856d2cfa87f2f171",
          "startHandle": 10,
          "endHandle": 13
        },
        {
          "type": "characteristic",
          "uuid": "e35c8910a0624e3f856d2cfa87f2f171",
          "properties": 18,
          "secure": 0,
          "startHandle": 11,
          "valueHandle": 12
        },
        {
          "type": "characteristicValue",
          "handle": 12,
        },
        {
          "type": "descriptor",
          "handle": 13,
          "uuid": "2902",
          "properties": 14,
          "secure": 0,
        }
