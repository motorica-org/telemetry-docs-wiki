Mechanical
==========

.. code:: javascript

        {
                msgtype: "motorica-org.mechanical.v1.flex", // do we really need msgtype versioning here?
                content: {
                        body: str, // a fallback text for regular clients
                        timestamp: UnixTimestampInMilliseconds (ufloat64),
                        power: u8,
                }
        }
